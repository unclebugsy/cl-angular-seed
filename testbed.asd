;;;; testbed.asd

(asdf:defsystem #:testbed
  :description "Describe testbed here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :serial t
  :depends-on (#:hunchentoot
               #:parenscript
               #:cl-who
               #:css-lite
               #:jsown
               #:web-utils)
  :components ((:file "package")
               (:file "testbed")))
