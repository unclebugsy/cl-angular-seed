
(in-package #:testbed)

#| This lets us add empty tags to elements for angular. |#
(setf (html-mode) :sgml)

(defparameter *listener* nil)

(defun stop-server ()
  (when *listener*
    (hunchentoot:stop *listener*)
    (setf *listener* nil)))

(defun start-server (&key (port 8080))
  (unless *listener*
    (setf hunchentoot:*show-lisp-errors-p* t)
    (set-dispatch-table)
    (setq *listener* (make-instance 'hunchentoot:easy-acceptor
                                    :document-root (resource-path "./static")
                                    :port port))
    (hunchentoot:start *listener*)))

(defun set-dispatch-table ()
  (setf *dispatch-table* (list
                          (create-regex-dispatcher "^/testbed/css$" 'site-css)
                          #'dispatch-easy-handlers)))


(defmacro with-page (&rest body)
  `(cl-who:with-html-output-to-string
       (*standard-output* nil :prologue nil :indent t)
     (htm
      (:html :lang "en" :ng-app "myApp" :class "no-js"
        (:head
          (:meta :charset "utf-8")
          (:meta :name "description" :content "This is a description.")
          (:meta :name "viewport" :content "width=device-width, initial-scale=1.0")
          (:link :type "text/css" :rel "stylesheet" :href "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css")
          (:link :type "text/css" :rel "stylesheet" :href "http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css")
          (:link :type "text/css" :rel "stylesheet" :href "http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800")
          (:link :type "text/css" :rel "stylesheet" :href "/testbed/css")
          (:script :src "https://code.jquery.com/jquery-2.1.3.min.js")
          (:script :src "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js")
          (:script :src "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular.min.js")
          (:script :src "https://code.angularjs.org/1.3.13/angular-route.js")
          (:script :src "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"))
        
        (:body
          ,@body)))))

(defmacro with-ajax (&rest body)
  `(progn
     (no-cache)
     (cl-who:with-html-output-to-string
         (*standard-output* nil :prologue t :indent nil)
       (htm ,@body))))

(defmacro with-javascript (&rest body)
  `(progn
     (no-cache)
     (setf (hunchentoot:content-type*) "text/javascript")
     ,@body))

(defun site-css ()
  (setf (hunchentoot:content-type*) "text/css")
  (css-lite:css
    (("*") (:font-family "Open Sans"))
    ((".menu") (:list-style "none"
                :border-bottom "0.1em solid black"
                :margin-bottom "2em"
                :padding "0 0 0.5em"))
    ((".menu:before") (:content "["))
    ((".menu:after") (:content "]"))
    ((".menu > li") (:display "inline"))
    ((".menu > li:before") (:content "|"
                            :padding-right "0.3em"))
    ((".menu > li:nth-child(1):before") (:content ""
                                         :padding "0"))))


(define-easy-handler (index-handler :uri "/") ()
  (with-page
      (:div :class "container"
        (:div :class "row"
          (:div :class "col-md-12"
            (:ul :class "menu"
              (:li (:a :href "#/view1" "view1"))
              (:li (:a :href "#/view2" "view2")))
            (:div :ng-view t)
            (:div "Angular seed app: v"(:span :app-version t))))
        (:script :src "/d/app.js")
        (:script :src "/d/view1.js")
        (:script :src "/d/view2.js")
        (:script :src "/d/version.js")
        (:script :src "/d/version-directive.js")
        (:script :src "/d/interpolate-filter.js"))))

(define-easy-handler (dynjs-app :uri "/d/app.js") ()
  (with-javascript
      (ps (chain angular
                 (module "myApp" (array
                                  "ngRoute"
                                  "myApp.view1"
                                  "myApp.view2"
                                  "myApp.version"))
                 (config (array "$routeProvider" (lambda ($route-provider)
                                                   (chain $route-provider (otherwise (create redirect-to "/view1"))))))))))

(define-easy-handler (view-one-handler :uri "/view1") ()
  (with-ajax (htm (:p "This is the partial for view1"))))

(define-easy-handler (view-one-js :uri "/d/view1.js") ()
  (with-javascript
      (ps (chain angular
                 (module "myApp.view1" (array "ngRoute"))
                 (config (array "$routeProvider" (lambda ($route-provider)
                                                   (chain $route-provider
                                                          (when "/view1" (create template-url "/view1"
                                                                                 controller "ViewOneController"))))))
                 (controller "ViewOneController" (array (lambda () t)))))))

(define-easy-handler (view-two-handler :uri "/view2") ()
  (with-ajax (htm (:p "This is the partial for view2")
                  (:p "The 'interpolation' filter: {{ 'Current version is v%VERSION%.' | interpolate }}"))))

(define-easy-handler (view-two-js :uri "/d/view2.js") ()
  (with-javascript
      (ps (chain angular
                 (module "myApp.view2" (array "ngRoute"))
                 (config (array "$routeProvider" (lambda ($route-provider)
                                                   (chain $route-provider
                                                          (when "/view2" (create template-url "/view2"
                                                                                 controller "ViewTwoController"))))))
                 (controller "ViewTwoController" (array (lambda () t)))))))

(define-easy-handler (version-js :uri "/d/version.js") ()
  (with-javascript
      (ps
        (chain angular
               (module "myApp.version" (array
                                        "myApp.version.interpolate-filter"
                                        "myApp.version.version-directive"))
               (value "version" "0.1")))))

(define-easy-handler (version-directive-js :uri "/d/version-directive.js") ()
  (with-javascript
      (ps
        (chain angular
               (module "myApp.version.version-directive" (array))
               (directive "appVersion" (array "version" (lambda (version)
                                                          (lambda (scope elm attrs)
                                                            (chain elm (text version))
                                                            nil))))))))

(define-easy-handler (interpolate-filter-js :uri "/d/interpolate-filter.js") ()
  (with-javascript
      (ps
        (chain angular
               (module "myApp.version.interpolate-filter" (array))
               (filter "interpolate" (array "version" (lambda (version)
                                                        (lambda (text)
                                                          (chain (-string text)
                                                                 (replace (regex "/\%VERSION\%/mg") version))))))))))
